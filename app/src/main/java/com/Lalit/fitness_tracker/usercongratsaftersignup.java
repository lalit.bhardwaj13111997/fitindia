package com.Lalit.fitness_tracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



public class usercongratsaftersignup extends AppCompatActivity {
    @Override
    public void onBackPressed()
    {
        Intent it = new Intent(usercongratsaftersignup.this, MainActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(it);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usercongratsaftersignup);
        Button b=(Button)findViewById(R.id.mainbutton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(usercongratsaftersignup.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}